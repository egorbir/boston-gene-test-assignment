import pandas as pd

from app.database import engine

data = pd.read_csv("signatures.tsv", sep="\t", )
data = data.rename(columns={'Unnamed: 0': 'name'})

data.to_sql('t_signatures', engine, if_exists='append', index=False)
