from pydantic import BaseModel


class Signature(BaseModel):
    id: int
    name: str
    MHCI: float
    MHCII: float
    Coactivation_molecules: float
    Effector_cells: float
    T_cell_traffic: float
    NK_cells: float
    T_cells: float
    B_cells: float
    M1_signatures: float
    Th1_signature: float
    Antitumor_cytokines: float
    Checkpoint_inhibition: float
    Treg: float
    T_reg_traffic: float
    Neutrophil_signature: float
    Granulocyte_traffic: float
    MDSC: float
    MDSC_traffic: float
    Macrophages: float
    Macrophage_DC_traffic: float
    Th2_signature: float
    Protumor_cytokines: float
    CAF: float
    Matrix: float
    Matrix_remodeling: float
    Angiogenesis: float
    Endothelium: float
    Proliferation_rate: float
    EMT_signature: float

    class Config:
        orm_mode = True
