from sqlalchemy.orm import Session

from app import models


def get_signature(db: Session, sign_id: int):
    return db.query(models.Signature).filter(models.Signature.id == sign_id).first()


def get_signature_list(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Signature).offset(skip).limit(limit).all()
