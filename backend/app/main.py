from typing import List

from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session
from fastapi.middleware.cors import CORSMiddleware

from app import models, schemas, crud
from app.database import engine, SessionLocal

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


@app.get("/")
def index():
    return {"message": "Test assignment of BostonGene. Made by Egor Biryukov"}


@app.get("/signatures/", response_model=List[schemas.Signature])
def get_signature_list(skip: int = 0, limit: int = 100, db: Session() = Depends(get_db)):
    sign_list = crud.get_signature_list(db=db, skip=skip, limit=limit)
    return sign_list


@app.get("/signatures/{sign_id}", response_model=schemas.Signature)
def get_signature(sign_id: int, db: Session = Depends(get_db)):
    signature = crud.get_signature(db=db, sign_id=sign_id)
    return signature
