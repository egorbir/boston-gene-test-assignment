#!/bin/bash

rm -f alembic/versions/*
pipenv run alembic revision --autogenerate -m "Add tables"
pipenv run alembic upgrade head
pipenv run python ensure_populated.py
pipenv run python main.py
