import React, {useState} from 'react';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ModalInfo from "./ModalInfo";

function SignatureItem(props) {
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false);
    }

    const handleShow = () => {
        setShow(true);
    }

    return(
        <>
            <Row className="mb-3">
                <Card style={{width: '100%'}}>
                    <Card.Body>
                        <Card.Title>Patient: {props.signature.name}</Card.Title>
                        <Card.Text>
                            Signatures of patient {props.signature.name}
                        </Card.Text>
                        <Button href="#" onClick={handleShow}>Expand signatures</Button>
                    </Card.Body>
                </Card>
            </Row>
            <ModalInfo showModal={show} closeModal={handleClose} signature={props.signature}/>
        </>
    );
}

export default SignatureItem;