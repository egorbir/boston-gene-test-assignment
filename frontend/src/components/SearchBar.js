import React from 'react';
import Row from 'react-bootstrap/Row';

function SearchBar(props) {
    return(
        <Row className="mb-5 px-3">
            <label htmlFor="search">Search for patient</label>
            <input className="form-control" id="search" placeholder="Input name" type="text" onChange={props.onSearchChange}/>
        </Row>
    );
}

export default SearchBar