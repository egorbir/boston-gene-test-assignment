import React from 'react';
import SignatureItem from "./SignatureItem";

function SignatureList(props) {
    if (!props.data) {
        return(
            <p>Loading</p>
        );
    }

    const items = props.data.map((signature, index) => {
        return(
            <SignatureItem signature={signature} key={index} />
        );
    });

    return(
        <div className="px-3">
            {items}
        </div>
    );
}

export default SignatureList;