import React from 'react';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';

function ModalInfo(props) {
    const content = Object.keys(props.signature)
        .filter(key => key !== "id" && key !== "name")
        .map((key, i) => {
        return(
            <Row key={i}>
                <Col xs={5}><p className="h-5"><strong>{key}:</strong></p></Col>
                <Col xs={7}><p className="h-5">{props.signature[key]}</p></Col>
            </Row>
        );
    });

    return(
        <Modal show={props.showModal} onHide={props.closeModal} animation={false} size="xl">
            <Modal.Header closeButton>
                <Modal.Title>{props.signature.name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={props.closeModal}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default ModalInfo