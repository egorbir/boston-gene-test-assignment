import React, {Component} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SignatureList from "./components/SignatureList";
import SearchBar from "./components/SearchBar";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            signatures: null,
            active: null
        };
    }

    componentDidMount() {
        fetch('http://localhost:8000/signatures/')
            .then(res => res.json())
            .then((data) => {
                this.initialSignatures = data;
                this.setState({signatures: this.initialSignatures})
            });
    }

    onSearchChange(value) {
        const search = value.toLowerCase();
        const result = this.initialSignatures.filter(signature => {
            return(
                signature.name.toLowerCase().includes(search)
            );
        });

        this.setState({signatures: result});
    }

    render() {
        return (
            <>
                <Navbar bg="light" expand="lg">
                    <Navbar.Brand>Test Assignment</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                </Navbar>
                <Container fluid className="p-4">
                    <Row className="p-4 align-items-center justify-content-between">
                        <Col className="col-12 m-2">
                            <SearchBar onSearchChange={event => {this.onSearchChange(event.target.value)}}/>
                            <SignatureList data={this.state.signatures} />
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default App;
